from flask import Flask, jsonify, request
from flask_restful import reqparse
import werkzeug
import requests
from functools import wraps
import jwt

parser = reqparse.RequestParser()

# From file uploads
parser.add_argument('detection', type=werkzeug.datastructures.FileStorage, location='files',
                    help='Image file (.jpg, .jpeg, .png ...)')
parser.add_argument('recognition', type=werkzeug.datastructures.FileStorage, location='files',
                    help='The result json file of detection module')
parser.add_argument('translation', type=werkzeug.datastructures.FileStorage, location='files',
                    help='The result json file of recognition module')

# init flask app
app = Flask(__name__)

secret_key = 'secret_key'   # third-party 툴로 관리되어야 합니다.


def jwt_token_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        # header에 토큰이 있는지
        # token이 정상적인지
        if not 'Authorization' in request.headers:
            return jsonify({
                'msg': 'token is not given',
            }), 400
        token = request.headers['Authorization']
        try:
            decoded_token = jwt.decode(token, secret_key, alhorithms=['HS256'])
        except:
            return jsonify({
                'msg': 'Invalida token given'
            }), 400
        kwargs['decoded_token'] = decoded_token
        return f(*args, **kwargs)
    return decorated_function


@app.route('/auth')
def auth():
    r = requests.get('http://127.0.0.1:5001/get_token') # auth은 auth-server에 토큰을 발급받음
    rsp_json = r.json()
    token = rsp_json['access_token']

    return jsonify({
        'access_token': token,
        'msg': 'Successfully issued token!'
    }), 200


@app.route('/detection', methods=['POST'])
@jwt_token_required
def detection_service(**kwargs):
    args = parser.parse_args(strict=True)
    r = requests.post('http://127.0.0.1:50051/detection', files={'input': args['detection'].stream})
    rsp_json = r.json()
    return jsonify(rsp_json), 200

# @app.route('/recognition',methods=['GET'])
# @jwt_token_required
# def recognition_service(**kwargs):
#     args = parser.parse_args(strict=True)
#     r = requests.get('http://127.0.0.1:50052/recognition?input=' + str(args['positions']))
#     rsp_json = r.json()
#     return jsonify(rsp_json), 200


@app.route('/recognition', methods=['POST'])
@jwt_token_required
def recognition_service(**kwargs):
    args = parser.parse_args(strict=True)
    r = requests.post('http://127.0.0.1:50052/recognition', files={'input': args['recognition'].stream})
    rsp_json = r.json()
    return jsonify(rsp_json), 200

# @app.route('/translation',methods=['GET'])
# @jwt_token_required
# def translation_service(**kwargs):
#     args = parser.parse_args(strict=True)
#     r = requests.get('http://127.0.0.1:50053/translation?input=' + str(args['sentence']))
#     rsp_json = r.json()
#     return jsonify(rsp_json), 200


@app.route('/translation', methods=['POST'])
@jwt_token_required
def translation_service(**kwargs):
    args = parser.parse_args(strict=True)
    r = requests.post('http://127.0.0.1:50053/translation', files={'input': args['translation'].stream})
    rsp_json = r.json()
    return jsonify(rsp_json), 200


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=50000)
