import torchvision.transforms as transforms
import numpy as np
from PIL import Image

normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                 std=[0.229, 0.224, 0.225])

loader = transforms.Compose([
    transforms.ToTensor(),  
    normalize, 
    transforms.Lambda(lambda image : image.unsqueeze(0))
    ])

def image_loader(iname, loader_mode):
    img = Image.open(iname).convert('RGB')
    h, w = img.size
    '''
    # SSD network requires a fixed size input.
    # For this model, a size of (1400x1400) is required.
    '''
    if h != 1400 and w != 1400:
        print('SSD network requires a fixed size input!. for this model, a size of (1400x1400) is required.')
        print('Thus, our detector resizes the input size.')
        img = img.resize((1400, 1400))
    
    img_cv2 = np.asarray(img.copy())
    img = loader(img)
    return img, img_cv2, iname.filename
