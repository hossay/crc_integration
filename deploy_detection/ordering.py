# coding: utf-8
import numpy as np
import operator

#MakelistNP 검출된 박스를 넘파이 배열을 받아서  sorting 된 넘파이 배열로 내보냄
class MakeListNP():

    def __init__(self, input_img_np, img_shape):   #input_img_np  : type은 numpy 일 것 !  인풋 1 : 박스 좌표들 넘파이 , 인풋 2 : 이미지 shape
        self.img_pre_list = input_img_np.tolist()           #img_pre_list : 인풋 넘파이를 리스트로 완전 변환. pre =prediction
        self.img_h = img_shape[1]
        
    def make_area_xywh(self):  # self.area_xywh_list 를 만든다. 리스트 안의 원소는 다음의 형태를 띈다. [박스크기, [x,y,w,h]]
    
        self.area_xywh_list = []
        for k,xyxy in enumerate(self.img_pre_list):   #xyxy  = [x,y,x,y] 의 형태로 박스 좌상단, 우하단의 좌표를 가리킨다.
            self.x1, self.y1, self.x2, self.y2 = xyxy
            self.w = self.x2 - self.x1
            self.h  = self.y2 - self.y1

            self.quadrangle = (self.x1+self.w)*(self.img_h - self.y1)   # (x1+w) * (img_height - y1)
            self.area_xywh_list.append([self.quadrangle, [self.x1, self.y1, self.w, self.h]])
 
    def make_group(self): 
        
        self.sorted_area_xywh_list = sorted(self.area_xywh_list, reverse=True)  #사각형 크기대로 줄서기

        self.mid = self.sorted_area_xywh_list[0][1][0] + self.sorted_area_xywh_list[0][1][2]/2    #[0][1][0] = x , [0][1][2] = w
        self.revision_factor = int(0.3 * self.sorted_area_xywh_list[0][1][2]/2)
        self.group_list = []
        self.not_group_list = []

        for k,box_xywh in enumerate(self.sorted_area_xywh_list):  #[1][0] = x , [1][2] = w ,
            self.x_1, self.y_1, self.x_2, self.y_2 =  box_xywh[1][0], box_xywh[1][1], box_xywh[1][2] + box_xywh[1][0], box_xywh[1][1] + box_xywh[1][3]
            if (self.x_2 > (self.mid - self.revision_factor)) == (self.x_1 < (self.mid + self.revision_factor) ):
                self.group_list.append([self.x_1, self.y_1, self.x_2, self.y_2])
            else :
                self.not_group_list.append(box_xywh)

        return sorted(self.group_list, key=operator.itemgetter(1)), sorted(self.not_group_list)


    def numbering(self):   #
        self.make_area_xywh() #내부 메소드 호출. 각 요소들을  [사각형 크기, [ x,y,w,h 좌표], ...] 의 형태로 self.area_xywh_list 에 반환
        self.group_dict = {} #dict 형태로 key 와 넘버링 라인별 묶음을 value로 저장
        self.dict_num = 0   #key 넘버를 0부터 주기 위한 것 

        while True:
            self.group_list , self.area_xywh_list = self.make_group() #내부 메소드 호출.  self.area_xywh_list = self.not_group_list 
            self.group_dict[self.dict_num] = self.group_list
            self.dict_num += 1
            if len(self.area_xywh_list) == 0 :
                break

        #print("key 값과 value 들  = " ,self.group_dict)
        
        #-------------------------------------------------------------------------------------------------------------------------------------------------------------
        self.order_key_list = []  # 무리 그룹들 각각의 x값을 기준으로 다시 재정렬한 key 값을 모은 리스트. (ex : key 넘버가 0, 1, 2, 순서대로 일때 x값 기준으로 key 들을 재정렬 하면 0, 16, 1 등...
        for j in sorted(self.group_dict.items(), key =operator.itemgetter(1) , reverse=True):
            self.order_key_list.append(j[0])
            
        #------------------------------------------------------------실제 순서대로 반환해 주기 --------------------------------------------------------------------
        self.true_order_boxes =[]
        for j in self.order_key_list:  # j  = key number 
            for i,xyxy in enumerate(self.group_dict[j]):
                self.true_order_boxes.append(xyxy)
        
#        print("넘버링된 한자 개수 = ", len(self.true_order_boxes))
#        print("들어온 input numpy 의 한자 개수 = " , len(self.img_pre_list)  )

    def get_boxes_numbered(self):
        return np.array(self.true_order_boxes) 

'''Test.  '''
#MLNP = MakeListNP(prediction_data, (1400, 1400, 3))

#MLNP.make_area_xywh()

#MLNP.numbering()
# Return
#numbered_boxes = MLNP.get_boxes_numbered()


