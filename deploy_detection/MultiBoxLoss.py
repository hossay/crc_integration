import torch
import torch.nn as nn
import torch.nn.functional as F
from box_utils import match, log_sum_exp


class MultiBoxLoss(nn.Module):
    def __init__(self, num_classes, overlap_thresh, negpos_ratio, which_gpu):
        super(MultiBoxLoss, self).__init__()
        self.num_classes = num_classes
        self.jaccard_thresh = overlap_thresh

        self.variance = [0.1, 0.2]

        self.negpos_ratio = negpos_ratio
        self.which_gpu = which_gpu

        self.which_epochs = 0
    
    def update_jaccard_thresh(self):
        self.jaccard_thresh += 0.05
        print('update the jaccard_thresh to : ', self.jaccard_thresh)

    def forward(self, predictions, targets):
        '''
        # predictions (tuple) : A tuple (loc, conf, prior boxes) from SSD net
            loc   : [batch_size, num_priors, 4]
            conf  : [batch_size, num_priors, num_classes]
            prior : [num_priors, 4]
        # targets : Ground truth boxes and labels for a batch,
            [num_objs, 5],
            [x1,y1,x2,y2,class] format
        '''
        mse_loss = nn.MSELoss()
        bce_loss = nn.BCELoss()

        loc_data, conf_data, priors = predictions

        # batch_size
        batch = loc_data.size(0)
        num_priors = priors.size(0)
        num_classes = self.num_classes
        '''
        Match priors and Ground Truth boxes
        '''
        loc_t = torch.Tensor(batch, num_priors, 4)
        conf_t = torch.LongTensor(batch, num_priors)

        for idx in range(batch):
            bboxes = targets[idx][:, :-1].data  # ground truth
            labels = targets[idx][:, -1].data  # ground truth
            defaults = priors.data  # priors

            match(self.jaccard_thresh,
                  bboxes, defaults,
                  self.variance, labels, loc_t, conf_t, idx
                  )
        loc_t = loc_t.cuda()
        conf_t = conf_t.cuda()

        # [batch, num_priors], not background element
        pos = conf_t > 0
        # candidate for (best_truth_overlap > jac_thresh)'s confidence
        # [1, 1]
        num_pos = pos.sum(1, keepdim=True)

        # Localization Loss (Smooth L1)
        # [batch, num_priors, 4], maybe by broad-casting
        pos_idx = pos.unsqueeze(pos.dim()).expand_as(loc_data)
        '''
        Location loss
        '''
        loc_p = loc_data[pos_idx].view(-1, 4)
        # location about encoded offsets
        loc_t = loc_t[pos_idx].view(-1, 4)

        loss_l = F.smooth_l1_loss(loc_p, loc_t, size_average=False)
        
        if num_pos.data.cpu().numpy().all() == 0:
            loss_l = loss_l.detach()  # Don't optimize

        '''
        Hard Negative Mining
        '''
        # Compute max conf across batch for hard negative mining
        # [8732*batch, num_class]
        batch_conf = conf_data.view(-1, num_classes)
        # [batch*num_priors, 1]
        loss_c = log_sum_exp(batch_conf) - batch_conf.gather(1,
                                                             conf_t.view(-1, 1))  # right-side : [num_priors, 1]
        # [batch, num_priors]
        loss_c = loss_c.view(batch, -1)
        loss_c[pos] = 0  # filter out pos boxes for now, for finding negative samples
        _, loss_idx = loss_c.sort(1, descending=True)  # sort() returns (sorted_tensor, sorted_indices)
        
        # [batch, num_priors]
        _, idx_rank = loss_idx.sort(1)

        # num_pos * 3
        num_neg = torch.clamp(torch.clamp(self.negpos_ratio * num_pos, max=pos.size(1) - 1), min=128)

        # neg = (loss_idx < num_neg).expand_as(loss_idx)
        neg = (idx_rank < num_neg).expand_as(idx_rank)

        # # Confidence Loss Including Positive and Negative Examples
        # [batch, num_priors, num_classes]
        pos_idx = pos.unsqueeze(2).expand_as(conf_data)
        neg_idx = neg.unsqueeze(2).expand_as(conf_data)

        conf_p = conf_data[(pos_idx + neg_idx).gt(0)].view(-1, num_classes)
        targets_weighted = conf_t[(pos + neg).gt(0)]

        loss_c = F.cross_entropy(conf_p, targets_weighted, size_average=False)

        # Sum of losses: L(x,c,l,g) = (Lconf(x, c) + αLloc(x,l,g)) / N
        N = num_pos.data.sum().float() + num_neg.data.sum().float()

        loss_l /= N
        loss_c /= N

        return loss_l, loss_c
