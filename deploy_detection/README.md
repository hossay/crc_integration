# About Detection module  

## Input  
- The detector model uses 1400x1400 as the input size. (If another size comes in, I forced it to resize it.)  
- Detection is good for sizes larger than 600 * 600, but not for sizes smaller than 600 * 600.  

## Output  
- Dictionary format.  
  * Previous Format : {  
'Detection coordinates': [],  
'Detection result image': byte array,  
'Detection numbering': [2,3,10,1, ...]  
}  
  * New Format : {  
 'Detection coordinates' : [],  
 'Detection result image (cropped)': numpy array of (N, H(=128), W(=128), C) shape.  
 'Detection numbering': [2,3,10,1, ...]  
}  

