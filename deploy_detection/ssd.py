import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.nn.init as init
import torchvision.models as models
from prior_box import PriorBox
from detection import Detect
#from visualize import visualize_filters

class SSD(nn.Module):
    def __init__(self, phase, base, extras, num_dbox, num_classes, nms_thresh, which_gpu):
        super(SSD, self).__init__()
        self.which_gpu = which_gpu
        self.phase = phase
        self.num_classes = num_classes
        self.num_dbox = num_dbox

        # Network Build.
        self.resnet = base
        self.extras = nn.ModuleList(extras)

        # Prediction
        self.loc = nn.Conv2d(self.extras[-1].out_channels, self.num_dbox * 4, kernel_size=3, padding=1)
        self.conf = nn.Conv2d(self.extras[-1].out_channels, self.num_dbox * self.num_classes, kernel_size=3, padding=1)

        self.softmax = nn.Softmax(dim=-1)

        self.prior_layers = [[1, 80, 88, 88]]
        self.inp_size = [1, 3, 1400, 1400]

        self.priorbox = PriorBox(self.prior_layers, self.inp_size, self.which_gpu)
        self.priors = self.priorbox.forward().cuda().type(torch.cuda.FloatTensor)
        
        # Detection for evaluation
        self.nms_thresh = nms_thresh
        self.conf_thresh = 0.1
        self.top_k = 1000
        self.detect = Detect(num_classes, 0, self.top_k, self.conf_thresh, self.nms_thresh)

    def forward(self, x):
        loc, conf, prior_layers = list(), list(), list()

        # Forward to base network
        x = self.resnet(x)
        for k, v in enumerate(self.extras):
            x = F.relu(v(x), inplace=True)
            # Forwarding extra layers
            # In last layer, Do prediction
            if k == len(self.extras) - 1:
                # final feature size : [1, 128(=c), 88(=h), 88(=w)]
                loc.append(self.loc(x).permute(0, 2, 3, 1).contiguous())
                conf.append((self.conf(x).permute(0, 2, 3, 1).contiguous()))

        # num_priors Detections per Class
        # [batch, num_priors*4 (=x,y,w,h)]
        loc = torch.cat([o.view(o.size(0), -1) for o in loc], 1)
        # [batch, num_priors*num_class]
        conf = torch.cat([o.view(o.size(0), -1) for o in conf], 1)
        # 
        if self.phase == 'train':
            output = (
                loc.view(loc.size(0), -1, 4),
                conf.view(conf.size(0), -1, self.num_classes),
                self.priors
            )
        else:
            output = self.detect(
                loc.view(loc.size(0), -1, 4),
                self.softmax(conf.view(conf.size(0), -1, self.num_classes)),
                self.priors.type(x.data.type())
            )
#        visualize_filters(self.loc, self.conf)

        return output

def get_resnet(which_gpu, pivot):
    """
    # pivot -2 : size scale 1/32, ch : 512
    # pivot -3 : size scale 1/16, ch : 256
    # pivot -4 : size scale 1/8, ch : 128
    # pivot -5 : size scale 1/4
    """
    resnet = models.resnet34(pretrained=True)

    resnet_list = list(resnet.children())
    if pivot == -2:
        resnet_ch = 512
    elif pivot == -3:
        resnet_ch = 256
    elif pivot == -4:
        resnet_ch = 128
    elif pivot == -5:
        resnet_ch = 64
    return nn.Sequential(*resnet_list[:pivot]), resnet_ch

def add_extras(i, which_gpu):
    layers = []
    in_channels = i

    layers += [nn.Conv2d(in_channels, in_channels * 2, kernel_size=3, stride=1, padding=1)]
    layers += [nn.Conv2d(in_channels * 2, in_channels * 2, kernel_size=3, stride=1, padding=1)]
    layers += [nn.Conv2d(in_channels * 2, in_channels * 4, kernel_size=3, stride=1, padding=1)]
    layers += [nn.Conv2d(in_channels * 4, in_channels * 4, kernel_size=3, stride=1, padding=1)]
    layers += [nn.Conv2d(in_channels * 4, in_channels * 4, kernel_size=3, stride=1, padding=1)]
    layers += [nn.Conv2d(in_channels * 4, in_channels * 4, kernel_size=3, stride=1, padding=1)]
    layers += [nn.Conv2d(in_channels * 4, in_channels * 2, kernel_size=3, stride=1, padding=1)]
    layers += [nn.Conv2d(in_channels * 2, in_channels, kernel_size=3, stride=2, padding=1)]

    layers += [nn.Conv2d(in_channels, in_channels * 2, kernel_size=3, stride=1, padding=1)]
    layers += [nn.Conv2d(in_channels * 2, in_channels * 2, kernel_size=3, stride=1, padding=1)]
    layers += [nn.Conv2d(in_channels * 2, in_channels * 4, kernel_size=3, stride=1, padding=1)]
    layers += [nn.Conv2d(in_channels * 4, in_channels * 4, kernel_size=3, stride=1, padding=1)]
    layers += [nn.Conv2d(in_channels * 4, in_channels * 4, kernel_size=3, stride=1, padding=1)]
    layers += [nn.Conv2d(in_channels * 4, in_channels * 4, kernel_size=3, stride=1, padding=1)]
    layers += [nn.Conv2d(in_channels * 4, in_channels * 2, kernel_size=3, stride=1, padding=1)]
    layers += [nn.Conv2d(in_channels * 2, in_channels, kernel_size=3, stride=1, padding=1)]

    return layers


def build_ssd(phase, num_classes, nms_thresh, num_dbox, which_gpu):
    if phase != 'test' and phase != 'train':
        print('Error: Phase not recognized"')
        return
    resnet_layers, resnet_ch = get_resnet(which_gpu, -4)

    extra_layers = add_extras(resnet_ch, which_gpu)

    return SSD(phase, resnet_layers, extra_layers, num_dbox, num_classes, nms_thresh, which_gpu)
