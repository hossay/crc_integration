from flask import Flask
from flask_restful import Resource, Api, reqparse
from model import Model
import werkzeug.datastructures
import json
from PIL import Image
from io import BytesIO
import base64
import os
import cv2
import numpy as np

os.environ['CUDA_VISIBLE_DEVICES'] = '0'

# init model

model = Model()

parser = reqparse.RequestParser()
# parser.add_argument('input', type=str, help='json string of bbox positions')
parser.add_argument('input', type=werkzeug.datastructures.FileStorage, location='files')

app = Flask(__name__)
api = Api(app)


def get_cropped_images(img, coord):
    """
    :param img: Numpy array representing RGB image
    :param coord: coord: List of coordinate - List[(x1, y1, x2, y2), (int, int, int, int)]
    :return:
    """
    cropped_list = []
    crop_size = (224, 224)
    for x1, y1, x2, y2 in coord:
        cropped_list.append(cv2.resize(img[y1:y2, x1:x2, :], crop_size))
    cropped_imgs = np.asarray(cropped_list)
    return cropped_imgs


def str2img(img_str):
    """
    :param img_str: byte string image
    :return: PIL Image object
    """
    i = BytesIO(base64.b64decode(img_str))
    return Image.open(i)


class Service(Resource):
    def post(self):
        args = parser.parse_args(strict=True)
        json_file = args['input']
        json_file.save('detection.json')
        with open('detection.json') as j:
            data = json.loads(json.load(j))

        # Run the model by code input
        # return self.model.run(input=load_input)
        recons_image = str2img(data['origin'])
        crop_imgs = get_cropped_images(np.array(recons_image), data['coord'])

        # Run the model by args input
        return model.run(crop_imgs)


# /{...} : Enter service name in {...}
api.add_resource(Service, '/recognition')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=50052)