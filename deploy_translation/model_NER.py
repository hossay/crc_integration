from utils_NER import *
import tensorflow.contrib as tf_contrib
import time


class RnnNER:
    def __init__(self,
                 logdir: str,
                 layer_bme: int,
                 hidden_bme: int,
                 embed_size: int,
                 ch2idx: Dict,
                 bme2idx: Dict,
                 train_log='Train_log.txt',
                 test_log='Test_log.txt'):
        self.logdir = logdir
        os.makedirs(self.logdir, exist_ok=True)
        self.train_log = train_log
        self.test_log = test_log

        self.embed_size = embed_size

        self.layer_bme = layer_bme
        self.hidden_bme = hidden_bme

        self.ch2idx = ch2idx
        self.idx2ch = dict_swap(ch2idx)

        self.bme2idx = bme2idx
        self.idx2bme = dict_swap(bme2idx)

        self.in_dim = len(self.ch2idx)
        self.out_dim_bme = len(self.bme2idx)

        # [batch size, sequence length]
        self.x = tf.placeholder(tf.int32, [None, None], name='x_input')
        self.y_bme = tf.placeholder(tf.int32, [None, None], name='bme_target')
        self.wt = tf.placeholder(tf.float32, [None, None], name='loss_weight')
        self.seq_len = tf.placeholder(tf.int32, [None], name='seq_len')
        self.keep_prob = tf.placeholder(tf.float32, [], name='keep_prob')

        self.precision_entity = tf.placeholder(tf.float32, [])
        self.recall_entity = tf.placeholder(tf.float32, [])
        self.f1_score_entity = tf.placeholder(tf.float32, [])

        self.precision_surface = tf.placeholder(tf.float32, [])
        self.recall_surface = tf.placeholder(tf.float32, [])
        self.f1_score_surface = tf.placeholder(tf.float32, [])

        self.global_step = tf.get_variable('global_step', [],
                                           dtype=tf.int32,
                                           initializer=tf.initializers.constant(0),
                                           trainable=False)

        self.lr = tf.get_variable('learning_rate', [],
                                  dtype=tf.float32,
                                  initializer=tf.initializers.zeros(),
                                  trainable=False)
        self.lr_placeholder = tf.placeholder(tf.float32)
        self.lr_update = tf.assign(self.lr, self.lr_placeholder)

        self.embeddings = tf.Variable(tf.random_uniform([self.in_dim, self.embed_size], -1.0, 1.0))

        self.logit_bme = None
        self.trans_params = None
        self.loss_bme = None
        self._loss_bme = None
        self._loss_bme_op = None

        self.optimizer = None

        self.train_summaries = []
        self.train_merged = None
        self.test_summaries = []
        self.test_merged = None

        self.saver = None
        self.get_trainable_var = None
        self.global_var_init = None
        self.local_var_init = None
        return

    def net(self):
        # [batch size, time steps, input dimension]
        embed_x = tf.nn.embedding_lookup(self.embeddings, self.x)

        with tf.variable_scope('BME_model'):
            cell_fw_bme = [lstm_cell(self.hidden_bme, self.keep_prob) for _ in range(self.layer_bme)]
            cell_bw_bme = [lstm_cell(self.hidden_bme, self.keep_prob) for _ in range(self.layer_bme)]
            cell_out_bme, states_bme_fw, states_bme_bw = \
                tf_contrib.rnn.stack_bidirectional_dynamic_rnn(cell_fw_bme,
                                                               cell_bw_bme,
                                                               embed_x,
                                                               sequence_length=self.seq_len,
                                                               dtype=tf.float32)
            self.logit_bme = tf.layers.dense(cell_out_bme, self.out_dim_bme, activation=None)
        return

    def build(self):
        self.net()

        log_likelihood, trans_params = tf_contrib.crf.crf_log_likelihood(self.logit_bme, self.y_bme, self.seq_len)
        self.trans_params = trans_params
        self.loss_bme = tf.reduce_mean(-log_likelihood)

        self._loss_bme, self._loss_bme_op = tf.metrics.mean(self.loss_bme, name='loss_bme_mean')

        with tf.name_scope('accuracy'):
            self.test_summaries.append(tf.summary.scalar('precision_entity', self.precision_entity))
            self.test_summaries.append(tf.summary.scalar('recall_entity', self.recall_entity))
            self.test_summaries.append(tf.summary.scalar('f1-score_entity', self.f1_score_entity))

            self.test_summaries.append(tf.summary.scalar('precision_surface', self.precision_surface))
            self.test_summaries.append(tf.summary.scalar('recall_surface', self.recall_surface))
            self.test_summaries.append(tf.summary.scalar('f1-score_surface', self.f1_score_surface))

        with tf.name_scope('loss'):
            summary = tf.summary.scalar('loss_BME', self._loss_bme)
            self.train_summaries.append(summary)
            self.test_summaries.append(summary)
        self.train_summaries.append(tf.summary.scalar('learning rate', self.lr))

        self.optimizer = tf.train.AdamOptimizer(self.lr).minimize(self.loss_bme, self.global_step)

        self.train_merged = tf.summary.merge(self.train_summaries)
        self.test_merged = tf.summary.merge(self.test_summaries)
        self.saver = tf.train.Saver()
        self.get_trainable_var = tf.trainable_variables()
        self.global_var_init = tf.global_variables_initializer()
        self.local_var_init = tf.local_variables_initializer()
        return

    def init_model(self, sess, log_file=None, ckpt=None):
        if ckpt:
            self.saver.restore(sess, os.path.join(self.logdir, ckpt))
            if log_file:
                print_write('model loaded from file: %s\n' % os.path.join(self.logdir, ckpt),
                            os.path.join(self.logdir, log_file), 'a')
        else:
            f = open(os.path.join(self.logdir, log_file), 'w')
            sess.run(self.global_var_init)
            print_write('global variable initialize\n', f)
            writer = tf.summary.FileWriter(os.path.join(self.logdir, 'train'), filename_suffix='-graph')
            writer.add_graph(sess.graph)

            print_write('============================================================\n', f)
            count_vars = 0
            for var in self.get_trainable_var:
                name = var.name
                shape = var.shape.as_list()
                num_elements = var.shape.num_elements()
                print_write('Variable name: %s\n' % name, f)
                print_write('Placed device: %s\n' % var.device, f)
                print_write('Shape : %s  Elements: %d\n' % (str(shape), num_elements), f)
                print_write('============================================================\n', f)
                count_vars = count_vars + num_elements
            print_write('Total number of trainable variables %d\n' % count_vars, f)
            print_write('============================================================\n', f)
            f.close()
            writer.close()
        return

    def predict_batch(self, sess, word_ids, seq_lengths, keep_prob):
        viterbi_sequences = []
        feed = {self.x: word_ids, self.seq_len: seq_lengths, self.keep_prob: keep_prob}
        fetch = [self.logit_bme, self.trans_params]
        logits, trans_params = sess.run(fetch, feed_dict=feed)
        for logit, seq_len in zip(logits, seq_lengths):
            logit = logit[:seq_len]
            viterbi_seq, viterbi_score = tf_contrib.crf.viterbi_decode(logit, trans_params)
            viterbi_sequences += [viterbi_seq]
        return viterbi_sequences

    def train(self,
              sess: tf.Session,
              train_step: int,
              lr: float,
              train_data: List[LineData],
              test_data: Optional[List[LineData]],
              batch_size=128, keep_prob=0.5, ckpt=None, summary_step=100):
        assert (train_step % summary_step) == 0
        self.init_model(sess, self.train_log, ckpt)
        bucketted, bucket_prob = bucketting(train_data, BUCKET)

        global_step = sess.run(self.global_step)
        base_step = global_step
        sess.run([self.local_var_init, self.lr_update], feed_dict={self.lr_placeholder: lr})
        print_write('Step %d,  learning rate: %f,  time: %s\n'
                    % (global_step, sess.run(self.lr), time.strftime('%y-%m-%d %H:%M:%S')),
                    os.path.join(self.logdir, self.train_log), 'a')

        s = time.time()
        for i in range(train_step // (summary_step * 10)):
            writer = tf.summary.FileWriter(os.path.join(self.logdir, 'train'),
                                           filename_suffix='-step-%d' % global_step)
            for j in range(summary_step * 10):
                batch = get_batch(bucketted, bucket_prob, batch_size)

                feed = {self.x: batch.x_b, self.y_bme: batch.y_bme_b, self.wt: batch.loss_wt_b,
                        self.seq_len: batch.seq_len_b, self.keep_prob: keep_prob}
                fetch = [self.optimizer, self._loss_bme_op]
                _, loss_bme = sess.run(fetch, feed_dict=feed)
                global_step = sess.run(self.global_step)

                print('\rTraining - Loss_BME: %0.3f, step %d/%d'
                      % (loss_bme, global_step, train_step + base_step), end='')

                if global_step % summary_step == 0:
                    fetch = [self.train_merged, self._loss_bme]
                    merged, loss_bme = sess.run(fetch)
                    writer.add_summary(merged, global_step)

                    print('\r', end='')
                    print_write('Training - Loss_BME: %0.3f, step: %d, %0.2f sec/step\n'
                                % (loss_bme, global_step, (time.time() - s) / summary_step),
                                os.path.join(self.logdir, self.train_log), 'a')

                    s = time.time()
                    sess.run(self.local_var_init)

            print_write('global step: %d, model save, time: %s\n'
                        % (global_step, time.strftime('%y-%m-%d %H:%M:%S')),
                        os.path.join(self.logdir, self.train_log), 'a')
            self.saver.save(sess, os.path.join(self.logdir, 'weights'), global_step)
            if test_data is not None:
                self.eval(sess, test_data, batch_size)
            writer.close()
            s = time.time()
        return

    def eval(self,
             sess: tf.Session,
             data: List[LineData],
             batch_size: int,
             keep_prob=1.0, ckpt=None):
        if ckpt:
            self.init_model(sess, self.test_log, ckpt)
        global_step = sess.run(self.global_step)
        bucketted, bucket_prob = bucketting(data, BUCKET)
        n_data = len(data)
        pred_ne = []
        true_ne = []
        cnt = 0
        sess.run(self.local_var_init)
        s = time.time()
        for bucket in bucketted:
            len_bucket = len(bucket)
            for i in range(0, len_bucket, batch_size):
                try:
                    samples = bucket[i:i + batch_size]
                except IndexError:
                    samples = bucket[i:]
                batch = BatchData(samples)
                cnt += len(samples)
                feed = {self.x: batch.x_b, self.y_bme: batch.y_bme_b, self.wt: batch.loss_wt_b,
                        self.seq_len: batch.seq_len_b, self.keep_prob: keep_prob}
                fetch = self._loss_bme_op
                loss_bme = sess.run(fetch, feed_dict=feed)

                prediction = self.predict_batch(sess, batch.x_b, batch.seq_len_b, keep_prob)

                for k, sample in enumerate(samples):
                    pred_ne.append(pred2ne(sample.ch_line, prediction[k], BME2IDX))
                    true_ne.append(sample.entities)

                print('\rTesting - Loss_BME: %0.3f, step: %d, line %d/%d'
                      % (loss_bme, global_step, cnt, n_data), end='')

        f1_et, prcs_et, rcll_et = entity_form(true_ne, pred_ne)
        f1_sf, prcs_sf, rcll_sf = surface_form(true_ne, pred_ne)

        feed = {self.f1_score_entity: f1_et, self.precision_entity: prcs_et, self.recall_entity: rcll_et,
                self.f1_score_surface: f1_sf, self.precision_surface: prcs_sf, self.recall_surface: rcll_sf}

        fetch = [self.test_merged, self._loss_bme]
        merged, loss_bme = sess.run(fetch, feed_dict=feed)
        writer = tf.summary.FileWriter(os.path.join(self.logdir, 'test'),
                                       filename_suffix='-step-%d' % global_step)
        writer.add_summary(merged, global_step)
        writer.close()
        sess.run(self.local_var_init)

        print('\r', end='')
        print_write('Testing - Num_data: %d, Loss_BME: %0.3f\n'
                    'entity form[f1: %0.3f, precision: %0.3f, recall: %0.3f]\n'
                    'surface form[f1: %0.3f, precision: %0.3f, recall: %0.3f]\n'
                    'step: %d, time: %s\n'
                    % (n_data, loss_bme, f1_et, prcs_et, rcll_et, f1_sf, prcs_sf, rcll_sf,
                       global_step, time.strftime('%Hh %Mm %Ss', time.gmtime(time.time() - s))),
                    os.path.join(self.logdir, self.test_log), 'a')
        return

    def eval_linebyline(self,
                        sess: tf.Session,
                        data: List[LineData],
                        keep_prob=1.0, ckpt=None):
        if ckpt:
            self.init_model(sess, self.test_log, ckpt)
        global_step = sess.run(self.global_step)
        n_data = len(data)
        pred_ne = []
        true_ne = []
        sess.run(self.local_var_init)
        s = time.time()
        for i, d in enumerate(data):
            feed = {self.x: [d.x], self.y_bme: [d.y_bme], self.wt: [d.loss_wt],
                    self.seq_len: [d.seq_len], self.keep_prob: keep_prob}
            fetch = self._loss_bme_op
            loss_bme = sess.run(fetch, feed_dict=feed)

            prediction = self.predict_batch(sess, [d.x], [d.seq_len], keep_prob)

            pred_ne.append(pred2ne(d.ch_line, prediction[0], BME2IDX))
            true_ne.append(d.entities)

            print('\rTesting linebyline - Loss_BME: %0.3f, step: %d, line %d/%d'
                  % (loss_bme, global_step, i, n_data), end='')

        f1_et, prcs_et, rcll_et = entity_form(true_ne, pred_ne)
        f1_sf, prcs_sf, rcll_sf = surface_form(true_ne, pred_ne)

        feed = {self.f1_score_entity: f1_et, self.precision_entity: prcs_et, self.recall_entity: rcll_et,
                self.f1_score_surface: f1_sf, self.precision_surface: prcs_sf, self.recall_surface: rcll_sf}

        fetch = [self.test_merged, self._loss_bme]
        merged, loss_bme = sess.run(fetch, feed_dict=feed)
        writer = tf.summary.FileWriter(os.path.join(self.logdir, 'test_linebyline'),
                                       filename_suffix='-step-%d' % global_step)
        writer.add_summary(merged, global_step)
        writer.close()
        sess.run(self.local_var_init)

        print('\r', end='')
        print_write('Testing linebyline - Num_data: %d, Loss_BME: %0.3f\n'
                    'entity form[f1: %0.3f, precision: %0.3f, recall: %0.3f]\n'
                    'surface form[f1: %0.3f, precision: %0.3f, recall: %0.3f]\n'
                    'step: %d, time: %s\n'
                    % (n_data, loss_bme, f1_et, prcs_et, rcll_et, f1_sf, prcs_sf, rcll_sf,
                       global_step, time.strftime('%Hh %Mm %Ss', time.gmtime(time.time() - s))),
                    os.path.join(self.logdir, self.test_log), 'a')
        return

    def run(self, sess: tf.Session, sentence: str):
        length = len(sentence)
        x = np.zeros(length, dtype=np.int16)
        for i, c in enumerate(sentence):
            try:
                x[i] = self.ch2idx[c]
            except KeyError:
                x[i] = self.ch2idx['U']
        prediction = self.predict_batch(sess, [x], [length], 1.0)
        return pred2ne(sentence, prediction[0], self.bme2idx)
