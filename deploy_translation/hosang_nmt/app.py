from flask import Flask
from flask_restful import Resource, Api, reqparse
from model import Model

# init model
model = Model()

parser = reqparse.RequestParser()
parser.add_argument('input', type=str, help='input string without any whitespace')

app = Flask(__name__)
api = Api(app)

class Service(Resource):
    def get(self):
        args = parser.parse_args(strict=True)
        return model.run(args['input'])

# /{...} : Enter service name in {...}
api.add_resource(Service, '/translation')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=50053)