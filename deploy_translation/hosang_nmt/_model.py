import tensorflow as tf
from config import FLAGS
from helpers import *
import os
from tensorflow.python.layers.core import Dense

os.environ["CUDA_VISIBLE_DEVICES"]=FLAGS.gpu

class OutputLayer(tf.layers.Layer):
    def __init__(self, num_inputs, embedding_matrix):
        super(OutputLayer, self).__init__()
        self.num_inputs = num_inputs
        self.embedding_matrix = embedding_matrix
        self.shape = embedding_matrix.get_shape() # (vocab_size, embedding_dim)

    def build(self, _):
        embedding_size = self.embedding_matrix.get_shape()[1]
        self.kernel = self.add_variable("kernel", [self.num_inputs,
                                                   embedding_size])

    def call(self, inputs, **kwargs):
        hidden = tf.matmul(inputs, self.kernel)
        return tf.matmul(hidden, self.embedding_matrix, transpose_b=True)

    def _compute_output_shape(self, input_shape):
        input_shape = input_shape.with_rank_at_least(2)
        if input_shape[-1].value is None:
            raise ValueError(
              'The innermost dimension of input_shape must be defined, but saw: %s'
              % input_shape)
        return input_shape[:-1].concatenate(self.shape[0])



class TFModel(object):
    def __init__(self, hparams, ix2word):
        # hyper-params
        self.hparams = hparams
        self.ix2word = ix2word

        # placeholders
        self.x = tf.placeholder('int32', [FLAGS.batch_size, None])
        self.x_subword = tf.placeholder('int32', [FLAGS.batch_size, None, None])
        self.y = tf.placeholder('int32', [FLAGS.batch_size, None])
        self.learning_rate = tf.placeholder('float32')
        self.keep_prob = tf.placeholder('float32')

        def define_embedding_matrix(name, shape):
            e = tf.get_variable(name, shape)
            e = entry_stop_gradients(e,
                                     tf.stack([False] + (shape[0] - 1) * [True])[:, None])

            e_unpack = tf.unstack(e, num=shape[0])
            e_unpack[0] = tf.zeros(shape[1])

            return tf.stack(e_unpack)

        self.src_embeddings = tf.get_variable('src_embeddings', [hparams['src_vocab_size'], FLAGS.dim_emb])
        self.target_embeddings = tf.get_variable('target_embeddings', [hparams['target_vocab_size'], FLAGS.dim_emb])

        self.src_subword_embeddings = tf.get_variable('src_subword_embeddings', [hparams['src_subword_vocab_size'], FLAGS.dim_emb])

        # default : no-attention
        self.attention_func = None

        # specify attention mechanism
        if hparams.get('attention_mechanism')=='B':
            self.attention_func = tf.contrib.seq2seq.BahdanauAttention
        elif hparams.get('attention_mechanism')=='L':
            self.attention_func = tf.contrib.seq2seq.LuongAttention

        # Build seq2seq
        self.BuildSeq2Seq()


        # open session and init variables...
        config = tf.ConfigProto()
        config.gpu_options.allow_growth = True

        self.sess = sess = tf.Session(config=config)
        sess.run(tf.global_variables_initializer())

        # savers
        self.saver = saver = tf.train.Saver()

        pretrained_model_path = FLAGS.pretrained_model_path
        save_model_path = FLAGS.save_model_path

        if not FLAGS.fine_tune:
             save_model_path = pretrained_model_path

        if FLAGS.fine_tune:
            ckpt = tf.train.latest_checkpoint(pretrained_model_path)
            print('restore pre-trained params from {} ...'.format(os.path.splitext(ckpt)[0]))
            saver.restore(sess, ckpt)

        trace_ckpt = tf.train.latest_checkpoint(save_model_path)
        if trace_ckpt:
            print('restore all params from {} ...'.format(os.path.splitext(trace_ckpt)[0]))
            saver.restore(sess, trace_ckpt)

    def predict(self, feed_dict):
        generated_sent = self.sess.run(self.generated, feed_dict=feed_dict)
        hypo_processed = convert_to_sent(sent=generated_sent, ix2word=self.ix2word, is_postprocess=True)

        return dict(hypo=hypo_processed)

    def train_step(self, feed_dict):
        _, loss, step = self.sess.run([self.optimizer, self.loss, self.global_step], feed_dict=feed_dict)

        return dict(loss=loss, step=step)

    def eval_step(self, feed_dict):
        loss, generated_sent, step = self.sess.run([self.loss, self.generated, self.global_step], feed_dict=feed_dict)
        hypo_processed = convert_to_sent(sent=generated_sent, ix2word=self.ix2word, is_postprocess=True)

        return dict(loss=loss, hypo=hypo_processed, step=step)

    def save(self):
        pretrained_model_path = FLAGS.pretrained_model_path
        save_model_path = FLAGS.save_model_path

        if not FLAGS.fine_tune:
             save_model_path = pretrained_model_path

        path = os.path.join(save_model_path, 'step')

        # save trained model
        print('Save Model at {} ...'.format(path))
        self.saver.save(self.sess, path, global_step=self.global_step.eval(self.sess))


    def BuildSeq2Seq(self):
        # Encoder (bidirectional)
        encoder_cell_fw = RNNCellWrapper(num_units=FLAGS.num_units,
                                         num_layers=FLAGS.num_layers,
                                         keep_prob=self.keep_prob,
                                         attention_layer_size=FLAGS.num_units)

        encoder_cell_bw = RNNCellWrapper(num_units=FLAGS.num_units,
                                         num_layers=FLAGS.num_layers,
                                         keep_prob=self.keep_prob,
                                         attention_layer_size=FLAGS.num_units)

        # embedding layer for src language
        word_embed = tf.nn.embedding_lookup(self.src_embeddings, self.x)

        # embedding layer for src_subwords
        emb_seq = tf.nn.embedding_lookup(self.src_subword_embeddings, self.x_subword)

        # rnn layer for src_subwords...
        with tf.variable_scope('subword_rnn'):
            cell = tf.nn.rnn_cell.GRUCell(FLAGS.num_units)
            len_x_subword = tf.shape(emb_seq)[2]

            _in = tf.reshape(emb_seq, (-1, len_x_subword, FLAGS.dim_emb)) # (b*t_w, t_subw, dim_emb)
            subword_embed_seq, _ = tf.nn.dynamic_rnn(cell, _in, dtype=tf.float32) # (b*t_w, num_units)
            subword_embed = tf.reshape(subword_embed_seq[:,-1], (FLAGS.batch_size,-1,FLAGS.num_units))


        # subword_embed = tf.reduce_sum(
        #     tf.nn.embedding_lookup(self.src_subword_embeddings, self.x_subword), axis=2
        # )

        len_x = tf.reduce_sum(tf.sign(self.x), axis=1)

        # concat
        encoder_embed = tf.concat([word_embed, subword_embed], axis=-1)

        # RNN Encoder Network
        encoder_outputs, encoder_state = tf.nn.bidirectional_dynamic_rnn(cell_fw=encoder_cell_fw,
                                                                         cell_bw=encoder_cell_bw,
                                                                         inputs=encoder_embed,
                                                                         sequence_length=len_x,
                                                                         dtype='float32',
                                                                         scope='encoder')

        encoder_outputs_concat = tf.concat(encoder_outputs, axis=-1)

        attention_mechanism = None # default : None

        if self.attention_func:
            attention_mechanism = self.attention_func(num_units=FLAGS.num_units,
                                                      memory=encoder_outputs_concat,
                                                      memory_sequence_length=len_x)

        decoder_cell = RNNCellWrapper(num_units=FLAGS.num_units,
                                      num_layers=FLAGS.num_layers,
                                      keep_prob=self.keep_prob,
                                      attention=self.attention_func!=None, attention_mechanism=attention_mechanism,
                                      attention_layer_size=FLAGS.num_units)

        # decoding mask
        decoding_mask = tf.cast(tf.sign(self.y), 'float32')

        if self.hparams['is_train']:
            y_shifted = tf.concat([tf.fill([FLAGS.batch_size, 1], FLAGS.START), self.y[:, :-1]], 1)
            decoder_inp_embed = tf.nn.embedding_lookup(self.target_embeddings, y_shifted)

            # sequence_lengths for target language
            sequence_lengths = tf.cast(tf.reduce_sum(decoding_mask, axis=1), 'int32')

            # train helper (teaching for training mode)
            helper = tf.contrib.seq2seq.TrainingHelper(decoder_inp_embed, sequence_lengths)
        else:
            # greedy embedding helper (greedy decoding mode)
            helper = tf.contrib.seq2seq.GreedyEmbeddingHelper(self.target_embeddings,
                                                              tf.fill([FLAGS.batch_size], FLAGS.START),
                                                              FLAGS.END)

        # dense -> transpose embedding mat
        decoder_projection_layer = OutputLayer(num_inputs=FLAGS.num_units, embedding_matrix=self.target_embeddings)
        # decoder_projection_layer = Dense(self.hparams['target_vocab_size'], use_bias=False, name='decoder_projection')

        # decoder
        decoder = tf.contrib.seq2seq.BasicDecoder(
            decoder_cell, helper, initial_state=
            decoder_cell.zero_state(dtype='float32', batch_size=FLAGS.batch_size),
            output_layer=decoder_projection_layer)

        decoder_outputs, decoder_states, _ = tf.contrib.seq2seq.dynamic_decode(decoder,
                                                                               maximum_iterations=None if self.hparams['is_train'] else self.hparams['maxlen'])
        # logits
        logits = decoder_outputs.rnn_output

        # generated sentences
        self.generated = decoder_outputs.sample_id

        # loss
        crossent = tf.nn.softmax_cross_entropy_with_logits(logits=logits,
                                                           labels=tf.one_hot(self.y, self.hparams['target_vocab_size'], dtype='float32'))

        self.loss = tf.reduce_sum(crossent * decoding_mask) / tf.reduce_sum(decoding_mask)
        self.global_step = tf.Variable(0, trainable=False)

        # Adam
        self.optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate).minimize(self.loss, global_step=self.global_step)

