#-*- coding: utf-8 -*-
import numpy as np
import glob
import json
import os
from konlpy.tag import Mecab
from konlpy.utils import pprint
from collections import Counter, defaultdict
import re
import tqdm
from natsort import natsorted
import codecs
from config import FLAGS
import mojimoji
import subprocess

# regex to find NE part from target corpus
is_NE_target = re.compile(u'(\([\u2E80-\u2EFF\u3400-\u4DBF\u4E00-\u9FBF\uF900-\uFAFF]+\))')

# regex to find gangul chars
is_hangul = re.compile(u'[\u3131-\u314e\u314f-\u3163\uac00-\ud7a3]')

# regex to find hanja chars
is_hanja = re.compile(u'[\u2E80-\u2EFF\u3400-\u4DBF\u4E00-\u9FBF\uF900-\uFAFF]')

# parentheses
is_parentheses = re.compile(u'\(.*?\)')

# fuxxing trashes
src_trash_list = ['【', '[', '〔', '《', '〈',
                  '】', ']', '〕', '》', '〉',
                  '○', '/', ';', ':', '?', '!', '､',
                  '、', '‘', '’', 'ㆍ', '·', '"', "'",
                  ',','｡',':','"','!','?','·',';','(',')','､','○〕']

target_trash_list = ['【', '[', '〔', '《', '〈',
                     '】', ']', '〕', '》', '〉',
                     '○', '/', ';', ':', '､',
                     '、', '‘', '’', 'ㆍ', '·', '"', "'"]



def file_len(fname):
    p = subprocess.Popen(['wc', '-l', fname], stdout=subprocess.PIPE,
                                              stderr=subprocess.PIPE)
    result, err = p.communicate()
    if p.returncode != 0:
        raise IOError(err)
    return int(result.strip().split()[0])

def punctuation_splitter(sent):
    # Regular expressions used to tokenize.
    _WORD_SPLIT = re.compile(r"([.,!?\"':;)([])")

    words = []
    for word in sent.strip().split():
        words.extend(_WORD_SPLIT.split(word))

    return ' '.join([w for w in words if w])

def UnderSrcEntities(src, entities):
    for entity in entities:
        src_tokens = []
        for w in src.split():
            if entity in w and not '_' in w:
                src_tokens.append(w.replace(entity,' '+entity+'_'+' '))
            else:
                src_tokens.append(w)
        src = ' '.join(src_tokens)

    return src

def extract_entities_from_dataset(target):
    # find entities, and exclude them from POS tagging
    target_without_space = target.replace(' ', '')

    entities = is_NE_target.findall(target_without_space)

    # remove "( )"
    entities = list(map(lambda x: x.replace('(', ''), entities))
    entities = list(map(lambda x: x.replace(')', ''), entities))

    return entities

def extract_entities_with_dnn(src):
    # TODO. 1. should be converted to DL model at real time test phase...
    raise NotImplementedError("NER model should be implemented first!")

def cleaning_data(src_fn, target_fn,
                  src_out_fn='./data_new/sources.clean.hanja', target_out_fn='./data_new/targets.clean.kor'):

    assert file_len(src_fn)==file_len(target_fn)

    global src_trash_list, target_trash_list

    src_lines = list(map(lambda x: x.strip(), open(src_fn).readlines()))
    target_lines = list(map(lambda x: x.strip(), open(target_fn).readlines()))

    src_data = []
    target_data = []

    src_trash_list = list(map(lambda x: mojimoji.zen_to_han(x), src_trash_list))
    target_trash_list = list(map(lambda x: mojimoji.zen_to_han(x), target_trash_list))
    for src, target in tqdm.tqdm(zip(src_lines,target_lines)):
        # convert 전각->반각
        src = mojimoji.zen_to_han(src)
        target = mojimoji.zen_to_han(target)


        # cleaning before tokenization
        for trash in src_trash_list:
            src = src.replace(trash, '')

        # if any korean is included in src, throw it away !
        # and also if too long, throw it away(more than 100 chars)
        if is_hangul.findall(src) or len(src.replace(' ',''))>100:
            continue

        # preprocess target_sent(remove comments and any other noise)
        target = re.sub(r'\((\d*)\)', '', target)
        target = re.sub(r'【(.*)】|〈(.*)〉', '', target)
        target = re.sub(r'(\d\d\d|\d\d\d\d)].*', '', target)
        target = re.sub(r'(\d\d\d|\d\d\d\d)\)\s', '', target)
        target = target.replace('[','').replace(']','')

        # remove remaining trashes
        for trash in target_trash_list:
            target = target.replace(trash, '')
        if not src or not target:
            continue

        src_data.append(src+'\n'); target_data.append(target+'\n')

    with open(src_out_fn,'w') as f:
        f.writelines(src_data)

    with open(target_out_fn,'w') as f:
        f.writelines(target_data)


def preprocess_data(src_fn, target_fn, src_out_fn='./data_new/sources.tok.hanja', target_out_fn='./data_new/targets.tok.kor', mode='dataset'):
    assert mode in ['dataset', 'dnn'], "only support modes in [dataset, dnn]"

    assert file_len(src_fn)==file_len(target_fn)

    src_lines = list(map(lambda x: x.strip(), open(src_fn).readlines()))
    target_lines = list(map(lambda x: x.strip(), open(target_fn).readlines()))

    srcproc_data = []; targetproc_data = []

    for src, target in tqdm.tqdm(zip(src_lines,target_lines)):
        if mode=='dataset':
            # extract entities from dataset
            entities = extract_entities_from_dataset(target)
        elif mode=='dnn':
            entities = extract_entities_with_dnn(src)

        # underscore at the end of the each entity
        src = UnderSrcEntities(src, entities)

        target = is_parentheses.sub('', target)

        # remove all hanja from target
        target = is_hanja.sub('', target)

        # append data
        srcproc_data.append(src+'\n'); targetproc_data.append(target+'\n')

    with open(src_out_fn,'w') as f:
        f.writelines(srcproc_data)

    with open(target_out_fn,'w') as f:
        f.writelines(targetproc_data)

def bpe():
    '''
    Apply bpe code
    If vocab size is too big, try it!
    :return:
    '''
    pass


def preprocess(src_sent, word2ix):

    # step 1: cleaning
    src_sent = mojimoji.zen_to_han(src_sent)

    global src_trash_list

    src_trash_list = list(map(lambda x: mojimoji.zen_to_han(x), src_trash_list))
    for trash in src_trash_list:
        src_sent = src_sent.replace(trash, '')

    # step 2: NER -> separate NE
    # entities = extract_entities_with_dnn(src_sent)
    entities = ['f', 'u', 'c', 'k']

    # underscore at the end of the each entity with DNN
    src_sent = UnderSrcEntities(src_sent, entities)


    # step 3: word->ix, subword->ix

    # org : Token level
    # NE -> single NE word, non-NE -> split into subwords
    src_toks = []
    for src_tok in src_sent.strip().split():
        cur_tok = list(src_tok) if not '_' in src_tok else [src_tok]
        src_toks += cur_tok

    # novel : Subword level
    # NE -> split into list of subwords, non-NE -> unit sized-list of zero pad
    src_subword_ixs = []
    for tok in src_toks:
        if '_' in tok: # NE
            tok = tok.replace('_', '')
            src_subword_ixs.append([word2ix['src_subword'].get(c, FLAGS.UNK) for c in list(tok)])
        else:   # non-NE; zero pad
            src_subword_ixs.append([0])

    # remove all underscore
    src_toks = list(w.replace('_', '') for w in src_toks)

    return {
            'src_ixs': list(map(lambda tok: word2ix['src'].get(tok, FLAGS.UNK), src_toks)),
            'src_subword_ixs': src_subword_ixs
    }


def postprocess(target_sent):
    '''
    Remove '_'
    '''
    # step 1. replace double whitespace with special token '@'
    target_sent = target_sent.replace('  ', '@')

    # step 2. remove all single withspace
    target_sent = target_sent.replace(' ','')

    # step 2. replace '@' with whitespace
    target_sent = target_sent.replace('@',' ').strip()

    return target_sent



def shuffle(lines, seed=5):
    np.random.seed(seed)
    np.random.shuffle(lines)
    return lines

def split_dataset(dataset, split_ratio):
    n = len(dataset)
    train, valid, test = dataset[:int(n*split_ratio[0])],\
                         dataset[int(n*split_ratio[0]):int(-n*split_ratio[2])],\
                         dataset[int(-n*split_ratio[2]):]

    return train, valid, test

def create_vocab(lines, vocab_size, which):
    def stack_src_words(words, toks, entities):
        for tok in toks:
            if '_' in tok:
                tok = tok.replace('_','')
                words += [tok]     # NE
                entities += [tok]
            else:
                words += list(tok) # non-NE ; split into single char

    def stack_target_words(words, toks):
        words += toks

    ret_dict = {}
    ret_dict['entities'] = []
    words = []
    for line in lines:
        if which=='src':
            toks = line.strip().split()
            entities = []
            stack_src_words(words, toks, entities)
            ret_dict['entities'] += entities
        elif which=='target':
            toks = list(line.strip())
            stack_target_words(words,toks)

    words_count = Counter(words).most_common(vocab_size)
    common_words = list(map(lambda x:x[0], words_count))

    start, word2ix =  (2, {'<pad>': 0, '<unk>': 1}) if which=='src' else (4, {'<pad>': 0, '<unk>': 1, '<start>': 2, '<end>': 3})

    word2ix.update({ k:v for k,v in zip(common_words,range(start,vocab_size+start)) })

    ret_dict['word2ix'] = word2ix

    return ret_dict

def create_subword_vocab(entities, vocab_size):
    words = []
    for tok in entities:
        words += list(tok) # only consider subwords of NE

    words_count = Counter(words).most_common(vocab_size)
    common_words = list(map(lambda x:x[0], words_count))

    start, word2ix =  (2, {'<pad>': 0, '<unk>': 1})

    word2ix.update({ k:v for k,v in zip(common_words,range(start,vocab_size+start)) })

    return word2ix

def parse_dataset(src_lines, target_lines, split_ratio, vocab_size):
    n = len(src_lines)

    # create vodab(word2ix) for [train+valid] split
    src_vocab_size, target_vocab_size = vocab_size
    src_vocab_info = create_vocab(src_lines[:-int(n*split_ratio[2])], src_vocab_size, which='src')
    src_vocab = src_vocab_info.get('word2ix')
    src_subword_vocab = create_subword_vocab(list(set(src_vocab_info.get('entities'))), src_vocab_size)
    target_vocab = create_vocab(target_lines[:-int(n*split_ratio[2])], target_vocab_size, which='target').get('word2ix')
    word2ix = {'src':src_vocab, 'src_subword': src_subword_vocab,
               'target':target_vocab}  # integrated vocab named as word2ix

    # define dataset as below
    dataset = []
    for src, target in tqdm.tqdm(zip(src_lines, target_lines)):
        src_toks = []
        for src_tok in src.strip().split():
            cur_tok = list(src_tok) if not '_' in src_tok else [src_tok]
            src_toks += cur_tok

        # append <end> token at each EOS
        target_toks = list(target.strip()) + ['<end>']

        src_subword_ixs = []
        for tok in src_toks:
            if '_' in tok:
                tok = tok.replace('_','')
                src_subword_ixs.append([ word2ix['src_subword'].get(c, FLAGS.UNK) for c in list(tok) ])
            else:
                src_subword_ixs.append([0])

        src_toks = list(w.replace('_','') for w in src_toks)

        dataset.append({'src_sent': ' '.join(src_toks),
                        'target_sent': ''.join(target),
                        'src_ixs': list(map(lambda tok: word2ix['src'].get(tok, FLAGS.UNK), src_toks)),
                        'src_subword_ixs': src_subword_ixs,
                        'target_ixs': list(map(lambda tok: word2ix['target'].get(tok, FLAGS.UNK), target_toks))})

    # split dataset into train/valid/test
    train, valid, test = split_dataset(dataset, split_ratio)

    return train, valid, test, word2ix

def prepare_dataset(src_fn, target_fn,
                    trainset_fn='./data_new/train.json', validset_fn='./data_new/valid.json', testset_fn='./data_new/test.json',
                    vocab_size=(30000, 30000), split_ratio=(0.7, 0.1, 0.2), maxlen=100):

    assert file_len(src_fn)==file_len(target_fn)

    src_lines_org = list(map(lambda x: x.strip(), open(src_fn).readlines()))
    target_lines_org = list(map(lambda x: x.strip(), open(target_fn).readlines()))

    # filter by its length (maxlen = 100)
    src_lines = []
    target_lines = []
    for src,target in zip(src_lines_org,target_lines_org):
        if len(src.split()) > maxlen or len(target) > maxlen:
            continue
        src_lines.append(src)
        target_lines.append(target)

    # randomly shuffle
    src_lines = shuffle(src_lines)
    target_lines = shuffle(target_lines)

    # prepare dataset
    train, valid, test, word2ix = parse_dataset(src_lines, target_lines, split_ratio=split_ratio, vocab_size=vocab_size)

    # save datasets
    json.dump(train, open(trainset_fn, 'w'))
    json.dump(valid, open(validset_fn, 'w'))
    json.dump(test, open(testset_fn, 'w'))

    # save word2ix
    json.dump(word2ix, open('./data_new/word2ix.json', 'w'))


class Batcher(object):
    def __init__(self, dataset, batch_size):
        self.dataset = dataset
        self.batch_size = batch_size
        self.batch_generator = self.batch_generator()
        self.epoch = 0

    def batch_generator(self):
        start = 0
        while True:
            mini_batch = dict(src_sent=[], src_ixs=[], src_subword_ixs=[], target_sent=[], target_ixs=[])

            if start > len(self.dataset) - self.batch_size:
                ixs = np.arange(len(self.dataset))
                np.random.shuffle(ixs)
                self.dataset = list(np.array(self.dataset)[ixs])
                self.epoch += 1
                start = 0
                print('end of batch, shuffle...')

            cur_batch = self.dataset[start:start + self.batch_size]

            maxlen_src = max(len(sample['src_ixs']) for sample in cur_batch)
            maxlen_src_subword = max(max(len(e) for e in sample['src_subword_ixs']) for sample in cur_batch)
            maxlen_target = max(len(sample['target_ixs']) for sample in cur_batch)

            # padding with zero
            for sample in cur_batch:
                src_ix_padded = sample['src_ixs'] + [0] * (maxlen_src - len(sample['src_ixs']))
                src_subword_ix_padded = [ e+[0]*(maxlen_src_subword-len(e)) for e in sample['src_subword_ixs'] ]
                target_ix_padded = sample['target_ixs'] + [0] * (maxlen_target - len(sample['target_ixs']))
                mini_batch['src_sent'].append(sample['src_sent'])
                mini_batch['target_sent'].append(sample['target_sent'])
                mini_batch['src_ixs'].append(src_ix_padded)
                mini_batch['src_subword_ixs'].append(src_subword_ix_padded)
                mini_batch['target_ixs'].append(target_ix_padded)

            for i,sample in enumerate(mini_batch['src_subword_ixs']):
                src_subword_ix_padded = sample + [[0] * maxlen_src_subword] * (maxlen_src - len(sample))
                mini_batch['src_subword_ixs'][i] = src_subword_ix_padded

            start += self.batch_size

            yield mini_batch

    def next_batch(self):
        return next(self.batch_generator)


if __name__=='__main__':
    # step 1. Cleaning data
    cleaning_data(src_fn='./data_new/sources.raw.hanja', target_fn='./data_new/targets.raw.kor')

    # step 2. Preproces, tokenize + insert sepcial tokens
    preprocess_data(src_fn='./data_new/sources.clean.hanja', target_fn='./data_new/targets.clean.kor')

    # step 3. Apply BPE algorithm
    # Skip!

    # step 4. Create vocabulary and create dataset
    prepare_dataset(src_fn='./data_new/sources.tok.hanja', target_fn='./data_new/targets.tok.kor')