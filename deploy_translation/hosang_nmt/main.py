from config import FLAGS
import os
from data_utils import postprocess, Batcher, preprocess
from _model import TFModel
from helpers import *

def train(hparams, train_batcher, valid_batcher, ix2word):

    # define MyModel
    model = TFModel(hparams=hparams, ix2word=ix2word)

    # train loop
    running_train_loss = 0.0

    while True:
        try:
            train_batch = train_batcher.next_batch()
            train_feed = { model.x:train_batch['src_ixs'], model.x_subword:train_batch['src_subword_ixs'],
                           model.y:train_batch['target_ixs'],
                           model.learning_rate : 1e-4, model.keep_prob : 0.7 }

            res = model.train_step(train_feed)

            running_train_loss += res['loss']
            step = res['step']

            if step % 100 == 0:
                print('step : {0}, train_loss : {1:.4f}'.format(step, running_train_loss / 100))
                running_train_loss = 0.0

            if step % FLAGS.valid_step == 0:
                valid_batch = valid_batcher.next_batch()
                valid_feed = { model.x: valid_batch['src_ixs'], model.x_subword: valid_batch['src_subword_ixs'],
                               model.y: valid_batch['target_ixs'] , model.keep_prob : 1.0 }

                res_val = model.eval_step(valid_feed)

                print('@@@ step : {0}, valid_loss : {1:.4f}'.format(step, res_val['loss']))

            if step % FLAGS.save_ckpt_step == 0:
                model.save()

        except KeyboardInterrupt:
            model.save()



def eval(hparams, test_batcher, ix2word):

    if not os.path.exists('./results'):
        os.mkdir('./results')

    # define MyModel
    model = TFModel(hparams=hparams, ix2word=ix2word)

    datasetGold = dict(annotations=[])
    datasetHypo = dict(annotations=[])

    cnt = 0

    while True:
        try:
            cur_batch = test_batcher.next_batch()

            if test_batcher.epoch > 0:
                break

            eval_feed = {model.x: cur_batch['src_ixs'], model.x_subword:cur_batch['src_subword_ixs'],
                         model.keep_prob: 1.0}

            hypo = model.predict(eval_feed)['hypo']
            gold = convert_to_sent(cur_batch['target_sent'][0], ix2word=ix2word, is_postprocess=False)

            datasetGold['annotations'].append(dict(sentence_id=cnt, caption=gold))
            datasetHypo['annotations'].append(dict(sentence_id=cnt, caption=hypo))

            cnt += 1
            print(cnt)
        except KeyboardInterrupt:
            print('save results files...')
            json.dump(datasetGold, open('./results/gold_val.json', 'w'))
            json.dump(datasetHypo, open('./results/hypo_val.json', 'w'))

    print('save results files...')
    json.dump(datasetGold, open('./results/gold_val.json', 'w'))
    json.dump(datasetHypo, open('./results/hypo_val.json', 'w'))


def demo(hparams, ix2word):
    # define MyModel
    model = TFModel(hparams=hparams, ix2word=ix2word)

    while True:
        try:
            usr = input("Input : ").strip()

            data = preprocess(src_sent=usr, word2ix=word2ix)
            demo_feed = {model.x: [data['src_ixs']], model.x_subword: [data['src_subword_ixs']],
                         model.keep_prob: 1.0}
            res = model.predict(demo_feed)

            print('Output : ', res['hypo'])

            llprint("\n[Ctrl+C] to quit\n\n")
        except KeyboardInterrupt:
            break


if __name__ == '__main__':

    # load from files
    train_set = json.load(open('./data_new/train.json', 'r'))
    valid_set = json.load(open('./data_new/valid.json', 'r'))
    test_set = json.load(open('./data_new/test.json', 'r'))

    # load vocab
    word2ix, ix2word = load_vocab('./data_new/word2ix.json')

    hparams = dict(src_vocab_size=len(ix2word['src']), src_subword_vocab_size=len(ix2word['src_subword']),
                   target_vocab_size=len(ix2word['target']),
                   maxlen=100, attention_mechanism='B'
                   )

    train_batcher = Batcher(train_set, batch_size=FLAGS.batch_size)
    test_batcher = Batcher(test_set, batch_size=FLAGS.batch_size)
    valid_batcher = Batcher(valid_set, batch_size=FLAGS.batch_size)

    if FLAGS.mode=='train':
        hparams['is_train'] = True
        train(hparams, train_batcher, valid_batcher, ix2word=ix2word)
    if FLAGS.mode=='eval':
        hparams['is_train'] = False
        eval(hparams, test_batcher, ix2word=ix2word)
    if FLAGS.mode=='demo':
        hparams['is_train'] = False
        demo(hparams, ix2word=ix2word)