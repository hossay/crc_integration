import sys, os
module_path = os.path.abspath('./nmt')
sys.path.insert(0,module_path)

from _model import TFModel
from helpers import *
from data_utils import preprocess

class Model(object):
    def __init__(self):
        # load vocab
        self.word2ix, self.ix2word = word2ix, ix2word = load_vocab(os.path.join(module_path, './data_new/word2ix.json'))

        hparams = dict(src_vocab_size=len(ix2word['src']), src_subword_vocab_size=len(ix2word['src_subword']),
                       target_vocab_size=len(ix2word['target']),
                       maxlen=100, attention_mechanism='B',
                       is_train=False,
                       )

        # define Model
        self.model = TFModel(hparams=hparams, ix2word=ix2word)

    def run(self, input):
        data = preprocess(src_sent=input, word2ix=self.word2ix)
        data_16_src = [data['src_ixs'] for i in range(16)]
        data_16_src_sub = [data['src_subword_ixs'] for i in range(16)]
        #demo_feed = {self.model.x: [data['src_ixs']], self.model.x_subword: [data['src_subword_ixs']], self.model.keep_prob: 1.0}
        demo_feed = {self.model.x: data_16_src, self.model.x_subword: data_16_src_sub, self.model.keep_prob: 1.0}

        res = self.model.predict(demo_feed)

        print('Output : ', res['hypo'])

        llprint("\n[Ctrl+C] to quit\n\n")

        ###
        # sess.run(fetches, feed_dict={'input':input})
        ###

        # result example(i.e. Translation)
        result = {
            'data': res['hypo']
        }

        return result


if __name__ == "__main__":
    m = Model()
    m.run(input='fuck')
