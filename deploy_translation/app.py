from flask import Flask
from flask_restful import Resource, Api, reqparse
from model2 import *
import werkzeug.datastructures
import json
from model_NER import *
from model_chunking import *


os.environ['CUDA_VISIBLE_DEVICES'] = '0'

# init model
model = Translator(model_dir='Model2_Combined2_Custom_Fine', ckpt='step-250000', mecab_dic_path='mecab-ko-dic-custom')
# model = Translator(model_dir='Model2_Combined1_Basic_Fine', ckpt='step-250000', mecab_dic_path='mecab-ko-dic-basic')


# ck_graph = tf.Graph()                                           # make chunking graph
# ck_sess = tf.Session(graph=ck_graph, config=get_tf_config())    # make chunking session for chunking graph
# ck_ch2idx = pickle_load('chunking/ch2idx.p')                    # chinese to index for chunking
#
# with ck_graph.as_default():                                     # build chunking model in chunking graph
#     ck_model = RnnChunking('chunking', 2, 512, 300, ck_ch2idx, MARK2IDX, MARK_IDX2MARK_NAME)
#     ck_model.build()
#     ck_model.init_model(ck_sess, ckpt='weights-4000')           # load chunking model parameters
#
#
# ner_graph = tf.Graph()                                          # make NER graph
# ner_sess = tf.Session(graph=ner_graph, config=get_tf_config())  # make NER session for NER graph
# ner_ch2idx = pickle_load('NER/ch2idx.p')                        # chinese to index for NER
#
# with ner_graph.as_default():                                    # build NER model in NER graph
#     ner_model = RnnNER('NER', 2, 512, 600, ner_ch2idx, BME2IDX)
#     ner_model.build()
#     ner_model.init_model(ner_sess, ckpt='weights-5000')         # load NER model parameters


parser = reqparse.RequestParser()
# parser.add_argument('input', type=str, help='input string without any whitespace')
parser.add_argument('input', type=werkzeug.datastructures.FileStorage, location='files')

app = Flask(__name__)

api = Api(app)


class Service(Resource):
    def post(self):
        args = parser.parse_args(strict=True)
        json_file = args['input']
        json_file.save('recognition.json')

        with open('recognition.json') as j:
            data = json.loads(json.load(j))

        # ck_out = ck_model.run(ck_sess, data['chinese'], rm_mark=False)
        # ner_out = ner_model.run(ner_sess, ck_out)
        # print(ck_out)
        # print(ner_out)

        return model.run(data['chinese'])


# /{...} : Enter service name in {...}
api.add_resource(Service, '/translation')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=50053)
    del model

