from utils_chunking import *
import tensorflow.contrib as tf_contrib
import time


class RnnChunking:
    def __init__(self,
                 logdir: str,
                 layer: int,
                 hidden: int,
                 embed_size: int,
                 ch2idx: Dict,
                 mark2idx: Dict,
                 mark_name: Dict,
                 train_log='Train_log.txt',
                 test_log='Test_log.txt'):
        self.logdir = logdir
        os.makedirs(self.logdir, exist_ok=True)
        self.train_log = train_log
        self.test_log = test_log

        self.embed_size = embed_size

        self.layer = layer
        self.hidden = hidden

        self.ch2idx = ch2idx
        self.idx2ch = dict_swap(ch2idx)

        self.mark2idx = mark2idx
        self.idx2mark = dict_swap(mark2idx)
        self.mark_name = mark_name

        self.in_dim = len(self.ch2idx)
        self.out_dim = len(self.mark2idx)

        # [batch size, sequence length]
        self.x = tf.placeholder(tf.int32, [None, None], name='x_input')
        self.y = tf.placeholder(tf.int32, [None, None], name='y_target')
        self.wt = tf.placeholder(tf.float32, [None, None], name='loss_weight')
        self.seq_len = tf.placeholder(tf.int32, [None], name='seq_len')
        self.keep_prob = tf.placeholder(tf.float32, [], name='keep_prob')

        self.global_step = tf.get_variable('global_step', [],
                                           dtype=tf.int32,
                                           initializer=tf.initializers.constant(0),
                                           trainable=False)

        self.lr = tf.get_variable('learning_rate', [],
                                  dtype=tf.float32,
                                  initializer=tf.initializers.zeros(),
                                  trainable=False)
        self.lr_placeholder = tf.placeholder(tf.float32)
        self.lr_update = tf.assign(self.lr, self.lr_placeholder)

        self.embeddings = tf.Variable(tf.random_uniform([self.in_dim, self.embed_size], -1.0, 1.0), name='embed_matrix')

        self.logit = None
        self.loss = None
        self._loss = None
        self._loss_op = None
        self.pred = None
        self.acc = None
        self._acc = None
        self._acc_op = None
        self._class_acc = None
        self._class_acc_op = None

        self.optimizer = None

        self.train_summaries = []
        self.train_merged = None
        self.test_summaries = []
        self.test_merged = None

        self.saver = None
        self.get_trainable_var = None
        self.global_var_init = None
        self.local_var_init = None
        return

    def net(self):
        # [batch size, time steps, input dimension]
        embed_x = tf.nn.embedding_lookup(self.embeddings, self.x)

        with tf.variable_scope('model'):
            cell_fw = [lstm_cell(self.hidden, self.keep_prob) for _ in range(self.layer)]
            cell_bw = [lstm_cell(self.hidden, self.keep_prob) for _ in range(self.layer)]
            cell_out, states_fw, states_bw = \
                tf_contrib.rnn.stack_bidirectional_dynamic_rnn(cell_fw,
                                                               cell_bw,
                                                               embed_x,
                                                               sequence_length=self.seq_len,
                                                               dtype=tf.float32)
            self.logit = tf.layers.dense(cell_out, self.out_dim, activation=None)
        return

    def build(self):
        self.net()

        self.loss = tf.contrib.seq2seq.sequence_loss(logits=self.logit,
                                                     targets=self.y,
                                                     weights=self.wt,
                                                     average_across_timesteps=True,
                                                     average_across_batch=True)

        self.pred = tf.to_int32(tf.argmax(self.logit, axis=2))
        self.acc = tf.reduce_mean(tf.cast(tf.equal(self.pred, self.y), tf.float32))

        self._loss, self._loss_op = tf.metrics.mean(self.loss, name='loss_bme_mean')
        self._acc, self._acc_op = tf.metrics.mean(self.acc, name='acc_bme_mean')
        # self._class_acc, self._class_acc_op = per_class_acc(self.y, self.pred, self.out_dim, 'class_acc')

        with tf.name_scope('accuracy'):
            summary = tf.summary.scalar('accuracy', self._acc)
            self.train_summaries.append(summary)
            self.test_summaries.append(summary)

            # summary = tf.summary.scalar('avg_cls_acc', tf.reduce_mean(self._class_acc))
            # self.train_summaries.append(summary)
            # self.test_summaries.append(summary)
            # for i in range(self.out_dim):
            #     summary = tf.summary.scalar('acc_%s' % self.mark_name[i], tf.squeeze(self._class_acc[i]))
            #     self.train_summaries.append(summary)
            #     self.test_summaries.append(summary)

        with tf.name_scope('loss'):
            summary = tf.summary.scalar('loss', self._loss)
            self.train_summaries.append(summary)
            self.test_summaries.append(summary)
        self.train_summaries.append(tf.summary.scalar('learning rate', self.lr))

        self.optimizer = tf.train.AdamOptimizer(self.lr).minimize(self.loss, self.global_step)

        self.train_merged = tf.summary.merge(self.train_summaries)
        self.test_merged = tf.summary.merge(self.test_summaries)
        self.saver = tf.train.Saver()
        self.get_trainable_var = tf.trainable_variables()
        self.global_var_init = tf.global_variables_initializer()
        self.local_var_init = tf.local_variables_initializer()
        return

    def init_model(self, sess, log_file=None, ckpt=None):
        if ckpt:
            self.saver.restore(sess, os.path.join(self.logdir, ckpt))
            if log_file:
                print_write('model loaded from file: %s\n' % os.path.join(self.logdir, ckpt),
                            os.path.join(self.logdir, log_file), 'a')
        else:
            f = open(os.path.join(self.logdir, log_file), 'w')
            sess.run(self.global_var_init)
            print_write('global variable initialize\n', f)
            writer = tf.summary.FileWriter(os.path.join(self.logdir, 'train'), filename_suffix='-graph')
            writer.add_graph(sess.graph)

            print_write('============================================================\n', f)
            count_vars = 0
            for var in self.get_trainable_var:
                name = var.name
                shape = var.shape.as_list()
                num_elements = var.shape.num_elements()
                print_write('Variable name: %s\n' % name, f)
                print_write('Placed device: %s\n' % var.device, f)
                print_write('Shape : %s  Elements: %d\n' % (str(shape), num_elements), f)
                print_write('============================================================\n', f)
                count_vars = count_vars + num_elements
            print_write('Total number of trainable variables %d\n' % count_vars, f)
            print_write('============================================================\n', f)
            f.close()
            writer.close()
        return

    def train(self,
              sess: tf.Session,
              train_step: int,
              lr: float,
              train_data: List[LineData],
              test_data: Optional[List[LineData]],
              batch_size=128, keep_prob=0.5, ckpt=None, summary_step=100):
        assert (train_step % summary_step) == 0
        self.init_model(sess, self.train_log, ckpt)
        bucketted, bucket_prob = bucketting(train_data, BUCKET)

        global_step = sess.run(self.global_step)
        base_step = global_step
        sess.run([self.local_var_init, self.lr_update], feed_dict={self.lr_placeholder: lr})
        print_write('Step %d,  learning rate: %f,  time: %s\n'
                    % (global_step, sess.run(self.lr), time.strftime('%y-%m-%d %H:%M:%S')),
                    os.path.join(self.logdir, self.train_log), 'a')

        s = time.time()
        for i in range(train_step//(summary_step*10)):
            writer = tf.summary.FileWriter(os.path.join(self.logdir, 'train'),
                                           filename_suffix='-step-%d' % global_step)
            for j in range(summary_step*10):
                batch = get_batch(bucketted, bucket_prob, batch_size)

                feed = {self.x: batch.x_b, self.y: batch.y_b, self.wt: batch.loss_wt_b,
                        self.seq_len: batch.seq_len_b, self.keep_prob: keep_prob}
                fetch = [self.optimizer, self._loss_op, self._acc_op, self._class_acc_op]
                _, loss, acc, cls_acc = sess.run(fetch, feed_dict=feed)
                global_step = sess.run(self.global_step)

                _format, _arg = get_class_acc_print(cls_acc, self.mark_name)
                _format = '\rTraining - loss: %0.3f, accuracy: %0.3f, ' + _format + 'step: %d/%d'
                _arg = tuple([loss, acc] + _arg + [global_step, train_step+base_step])
                print(_format % _arg, end='')

                if global_step % summary_step == 0:
                    fetch = [self.train_merged, self._loss, self._acc, self._class_acc]
                    merged, loss, acc, cls_acc = sess.run(fetch)
                    writer.add_summary(merged, global_step)

                    _format, _arg = get_class_acc_print(cls_acc, self.mark_name)
                    _format = 'Training - loss: %0.3f, accuracy: %0.3f, ' + _format + 'step: %d, %0.2f sec/step\n'
                    _arg = tuple([loss, acc] + _arg + [global_step, (time.time() - s) / summary_step])

                    print('\r', end='')
                    print_write(_format % _arg, os.path.join(self.logdir, self.train_log), 'a')

                    s = time.time()
                    sess.run(self.local_var_init)

            print_write('global step: %d, model save, time: %s\n'
                        % (global_step, time.strftime('%y-%m-%d %H:%M:%S')),
                        os.path.join(self.logdir, self.train_log), 'a')
            self.saver.save(sess, os.path.join(self.logdir, 'weights'), global_step)
            if test_data is not None:
                self.eval(sess, test_data, batch_size)
            writer.close()
            s = time.time()
        return

    def eval(self,
             sess: tf.Session,
             data: List[LineData],
             batch_size: int,
             keep_prob=1.0, ckpt=None):
        if ckpt:
            self.init_model(sess, self.test_log, ckpt)
        global_step = sess.run(self.global_step)
        bucketted, bucket_prob = bucketting(data, BUCKET)
        n_data = len(data)
        cnt = 0
        sess.run(self.local_var_init)
        s = time.time()
        for bucket in bucketted:
            len_bucket = len(bucket)
            for i in range(0, len_bucket, batch_size):
                try:
                    samples = bucket[i:i + batch_size]
                except IndexError:
                    samples = bucket[i:]
                batch = BatchData(samples)
                cnt += len(samples)
                feed = {self.x: batch.x_b, self.y: batch.y_b, self.wt: batch.loss_wt_b,
                        self.seq_len: batch.seq_len_b, self.keep_prob: keep_prob}

                fetch = [self._loss_op, self._acc_op, self._class_acc_op, self.pred]
                loss, acc, cls_acc, p = sess.run(fetch, feed_dict=feed)
                global_step = sess.run(self.global_step)
                _format, _arg = get_class_acc_print(cls_acc, self.mark_name)
                _format = '\rTesting - loss: %0.3f, accuracy: %0.3f, ' + _format + 'step: %d, line %d/%d'
                _arg = tuple([loss, acc] + _arg + [global_step, cnt, n_data])
                print(_format % _arg, end='')

        fetch = [self.train_merged, self._loss, self._acc, self._class_acc]
        merged, loss, acc, cls_acc = sess.run(fetch)
        writer = tf.summary.FileWriter(os.path.join(self.logdir, 'test'),
                                       filename_suffix='-step-%d' % global_step)
        writer.add_summary(merged, global_step)
        writer.close()
        sess.run(self.local_var_init)

        _format, _arg = get_class_acc_print(cls_acc, self.mark_name)
        _format = 'Testing - loss: %0.3f, accuracy: %0.3f, ' + _format + 'step: %d, time: %s\n'
        _arg = tuple([loss, acc] + _arg + [global_step, time.strftime('%Hh %Mm %Ss', time.gmtime(time.time() - s))])

        print('\r', end='')
        print_write(_format % _arg, os.path.join(self.logdir, self.test_log), 'a')
        return

    def run(self, sess: tf.Session, sentence: str, rm_mark: bool):
        x, removed = sentence2input(sentence, self.ch2idx, rm_mark=rm_mark)
        # print(removed)
        prediction = sess.run(self.pred,
                              feed_dict={self.x: [x], self.seq_len: [len(x)], self.keep_prob: 1.0})
        prediction = prediction[0]
        output = ''
        for i in range(len(x)):
            if prediction[i] == self.mark2idx['N']:
                output = output + removed[i]
            else:
                output = output + removed[i] + self.idx2mark[prediction[i]]
        return output
